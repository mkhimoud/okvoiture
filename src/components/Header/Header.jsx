import React from 'react'
import { Container, Row, Col } from "reactstrap";
import { Link, NavLink } from "react-router-dom";
import "../../styles/header.css";
import  { useRef } from "react";


const navLinks = [
  {
    path: "/home",
    display: "Home",
  },
  {
    path: "/RentMyCar",
    display: "RentMyCar",
  },
  {
    path: "/FindYourCar",
    display: "FindYourCar",
  },
  {
    path: "/About",
    display: "About",
  },
  {
    path: "/cars",
    display: "Cars",
  },

  {
    path: "/contact",
    display: "Contact",
  },
];
const Header = () => {
  const menuRef = useRef(null);

  const toggleMenu = () => menuRef.current.classList.toggle("menu__active");
  return (
    
          <header className="header">
            {/*  header top */}
            <div className="header_top">
              <Container>
                <Row>
                  <Col lg="6" md="6" sm="6">
                    <div className="header_top_left">
                      <span>Besoin d'aide?</span>
                      <span className="header_top_help">
                        <i class="ri-phone-fill"></i> +33-56-00-35-46
                      </span>
                      <span className="header_top_help">
                      <i class="ri-mail-line"></i>OKVOITURE@GMAIL.COM
                      </span>
                    </div>
                  </Col>
      
                  <Col lg="6" md="6" sm="6">
                    <div className="header_top_right d-flex align-items-center justify-content-end gap-3">
                      <Link to="#" className=" d-flex align-items-center gap-1">
                        <i class="ri-login-box-fill"></i> Login
                      </Link>
      
                      <Link to="#" className=" d-flex align-items-center gap-1">
                      <i class="ri-user-fill"></i> Register
                      </Link>
                    </div>
                  </Col>
                </Row>
              </Container>
            </div>

 {/*  header middle  */}

            <div className="header_middle">
              <Container>
                <Row>
                  <Col lg="3" md="3" sm="4">
                    <div className="logo">
                      <h1>
                        <Link to="/home" className=" d-flex align-items-center gap-2">
                        <i class="ri-roadster-fill"></i>
                          <span>
                            OkVoiture
                          </span>
                        </Link>
                      </h1>
                    </div>
                  </Col>
      
                  <Col lg="2" md="2" sm="2">
                    <div className="header_location d-flex align-items-center gap-2">
                      <span>
                      <i class="ri-map-pin-line"></i>
                      </span>
                      <div className="header_location-content">
                        <h4>France</h4>
                        <h6>Pris</h6>
                      </div>
                    </div>
                  </Col>
      
                  <Col lg="2" md="2" sm="2">
                    <div className="header_location d-flex align-items-center gap-2">
                      <span>
                        <i class="ri-time-line"></i>
                      </span>
                      <div className="header_location-content">
                        <h4>Sunday to Friday</h4>
                        <h6>10am - 7pm</h6>
                      </div>
                    </div>
                  </Col>
                  <Col lg="2" md="2" sm="2">
                    <div className="header_location d-flex align-items-center gap-2">
                      <span>
                      <i class="ri-showers-line"></i>
                      </span>
                      <div className="header_location-content">
                        <h4>Meteo</h4>
                        <h6>Paris - 7°</h6>
                      </div>
                    </div>
                  </Col>
      
                  <Col lg="2" md="3" sm="0"
                    className=" d-flex align-items-center justify-content-end "
                  >
                    <button className="header_btn btn ">
                      <Link to="/contact">
                        <i class="ri-phone-line"></i> Request a call
                      </Link>
                    </button>
                  </Col>
                </Row>
              </Container>
            </div>

            {/*  main navigation  */}

      <div className="main_navbar">
        <Container>
          <div className="navigation_wrapper d-flex align-items-center justify-content-between">
            <span className="mobile_menu">
              <i class="ri-menu-line" onClick={toggleMenu}></i>
            </span>

            <div className="navigation" ref={menuRef} onClick={toggleMenu}>
              <div className="menu">
                {navLinks.map((item, index) => (
                  <NavLink to={item.path} className={(navClass) =>
                      navClass.isActive ? "nav_active nav_item" : "nav_item"}
                      key={index}>{item.display}
                  </NavLink>
                ))}
              </div>
            </div>

            <div className="nav_right">
              <div className="search_box">
                <input type="text" placeholder="Search" />
                <span>
                  <i class="ri-search-line"></i>
                </span>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </header>

                      
  );
};

export default Header;






