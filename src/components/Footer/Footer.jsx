import React from 'react'
import { Container, Row, Col, ListGroup, ListGroupItem } from "reactstrap";
import { Link } from "react-router-dom";
import "../../styles/footer.css";


const Footer = () => {
  const date = new Date();
  const year = date.getFullYear();
  return (
    <div>
      <footer className="footer">
       <Container>
        <Row>
           <Col lg="4" md="4" sm="12">
            <div className="logo footer_logo">
              <h1>
                 <Link to="/home" className=" d-flex align-items-center gap-2">
                   <i class="ri-car-line"></i>
                   <span>
                     OkVoiture
                   </span>
                 </Link>
               </h1>
             </div>
             <p className="footer_logo-content">
               Lorem ipsum dolor sit amet consectetur adipisicing elit.
               Consequuntur, distinctio, itaque reiciendis ab cupiditate harum ex
               quam veniam, omnis expedita animi quibusdam obcaecati mollitia?
               Delectus et ad illo recusandae temporibus?
             </p>
           </Col>
          
          <Col lg="3" md="4" sm="6">
            <div className="mb-4">
              <h5 className="footer_link-title mb-4">Siège social</h5>
              <p className="office_info">Paris, France</p>
              <p className="office_info">Phone: +33 34 56 54 32</p>
              <p className="office_info">Email: okvoiture@gmail.com</p>
              <p className="office_info">heures de travails : 10am - 7pm</p>
            </div>
          </Col>

          <Col lg="3" md="4" sm="12">
            <div className="mb-4">
              <h5 className="footer_link-title">Newsletter</h5>
              <p className="section_description">Subscribe our newsletter</p>
              <div className="newsletter">
                <input type="email" placeholder="Email" />
                <span>
                  <i class="ri-send-plane-line"></i>
                </span>
              </div>
            </div>
          </Col>

          <Col lg="12">
            <div className="footer_bottom">
              <p className="section_description d-flex align-items-center justify-content-center gap-1 pt-4">
                <i class="ri-copyright-line"></i>Copyright {year}, Developed by
                Muhibur Rahman. All rights reserved.
              </p>
            </div>
          </Col>
            </Row>
            </Container>
            </footer>
    </div>
  )
}

export default Footer

