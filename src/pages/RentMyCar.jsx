import React from "react";
import "../styles/find-car-form.css";
import { Form, FormGroup } from "reactstrap";

const About = () => {
  return (
    <Form className="form">
        <div className=" d-flex align-items-center justify-content-between flex-wrap">
        <div><h1>information sur le proprietaire</h1></div>
                <FormGroup className="form_group">
                    
                    <input type="text" placeholder="Nom loueur" required />                           
                </FormGroup>
                <FormGroup className="form_group">  
                <input type="text" placeholder="Email" required />                
                </FormGroup> 
                            
                
            <FormGroup className="form_group">
                 <input type="text" placeholder="Marque" required />
            </FormGroup>          
            <FormGroup className="form_group">
                 <input type="text" placeholder="Année" required />
            </FormGroup>
            <FormGroup className="form_group">
                 <input type="text" placeholder="Ville" required />
            </FormGroup>
            <FormGroup className="form_group">
                 <input type="text" placeholder="Departement" required />
            </FormGroup>
            <FormGroup className="form_group">
            <input type="text" placeholder="Prix par jour" required />
            </FormGroup>        
            <FormGroup className="form_group">
                 <input type="text" placeholder="To address" required />
            </FormGroup>
            <FormGroup className="form_group">
                 <input type="date" placeholder="Journey date" required />
            </FormGroup>         
            <FormGroup className="select_group">
                <select>
                    <option value="Berline">Berline</option>
                    <option value="Coupé">Coupé</option>
                    <option value="Break">Break</option>
                    <option value="berline">berline</option>
                </select>
            </FormGroup>
            <FormGroup className="form_group">
                <h5>Joindre une photo de voiture</h5>
            <input type="file" required />
            </FormGroup>          
            <FormGroup className="form_group">
            <button className="btn find_car-btn">submit</button>
            </FormGroup>
      </div>
    </Form>   
  );
};

export default About;
