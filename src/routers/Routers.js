import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Home from "../pages/Home";
import About from "../pages/About";
 import NotFound from "../pages/NotFound";
import Contact from "../pages/Contact";
import RentMyCar from "../pages/RentMyCar"
import FindYourCar from "../pages/FindYourCar"

const Routers = () => {
  return (
    <Routes>
    <Route path="/" element={<Navigate to="/home" />} />
    <Route path="/home" element={<Home />} />
    <Route path="/RentMyCar" element={<RentMyCar />} />
    <Route path="/FindYourCar" element={<FindYourCar />} />
    <Route path="/about" element={<About />} />
    <Route path="/contact" element={<Contact />} />
    <Route path="*" element={<NotFound />} />
  </Routes>
  );
};

export default Routers;
